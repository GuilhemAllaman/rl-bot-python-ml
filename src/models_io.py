import pickle
import os

models_directory = "./data/models"

def load_model(input_number, model_type:str, game_type:str, version:str):
    directory = os.path.join(models_directory, game_type + "/" + version)
    model_file_path =  os.path.join(directory, "model_" + model_type + "_input" + str(input_number) + ".sav")
    print("loading model : ", model_file_path)
    return pickle.load(open(model_file_path, 'rb'))
