from rlbot.agents.base_agent import BaseAgent, SimpleControllerState
from rlbot.utils.structures.game_data_struct import GameTickPacket
import pygame

class ImitatorBot(BaseAgent):

    def initialize_agent(self):

        self.controller_state = SimpleControllerState()
        
        pygame.init()
        pygame.joystick.init()
        self.controller = pygame.joystick.Joystick(0)
        self.controller.init()
        print('Listening on joystick :', self.controller.get_name())

    def update_human_inputs(self):

        pygame.event.pump()
        self.controller_state.throttle = - self.controller.get_axis(2)
        self.controller_state.steer = self.controller.get_axis(0)
        self.controller_state.pitch = self.controller.get_axis(1)
        self.controller_state.yaw = self.controller.get_axis(0)
        self.controller_state.roll = self.controller.get_button(5) - self.controller.get_button(4)
        self.controller_state.jump = (self.controller.get_button(0) == 1)
        self.controller_state.boost = (self.controller.get_button(1) == 1)
        self.controller_state.handbrake = (self.controller.get_button(2) == 1)

    def get_output(self, packet: GameTickPacket) -> SimpleControllerState:
        self.update_human_inputs()
        return self.controller_state
