import math
import numpy as np

import rlbot.utils.game_state_util as gs_util

class Vector3(gs_util.Vector3):

    def __init__(self, x, y, z):
        super(Vector3, self).__init__(x, y, z)
        self.nparray = np.array([x, y, z])

    @staticmethod
    def from_base(base:gs_util.Vector3):
        return Vector3(base.x, base.y, base.z)

    def plus(self, other:gs_util.Vector3):
        return Vector3(self.x+other.x, self.y+other.y, self.z+other.z)

    def minus(self, other:gs_util.Vector3):
        return Vector3(self.x-other.x, self.y-other.y, self.z-other.z)

    def distance_2d(self, other:gs_util.Vector3):
        return math.sqrt(math.pow(self.x-other.x, 2) + math.pow(self.y-other.y, 2))

    def distance_3d(self, other:gs_util.Vector3):
        return math.sqrt(math.pow(self.x-other.x, 2) + math.pow(self.y-other.y, 2) + math.pow(self.z-other.z, 2))

    def length_2d(self):
        return math.sqrt(math.pow(self.x,2) + math.pow(self.y,2))

    def length_3d(self):
        return math.sqrt(self.nparray.dot(self.nparray))

    def angle_2d(self, target:gs_util.Vector3):

        currentRad = math.atan2(self.y, self.x)
        idealRad = math.atan2(target.y, target.x)

        if (abs(currentRad - idealRad) > math.pi):
            if (currentRad < 0):
                currentRad += math.pi * 2
            if (idealRad < 0):
                idealRad += math.pi * 2

        return idealRad - currentRad

    def angle(self, other:gs_util.Vector3):

        v_self = np.array([self.x, self.y, self.z])
        v_other = np.array([other.x, other.y, other.z])

        mag_self = math.sqrt(v_self.dot(v_self))
        mag_other = math.sqrt(v_other.dot(v_other))

        n_self = np.array([ v_self[i]/mag_self  for i in range(len(v_self)) ])
        n_other = np.array([ v_other[i]/mag_other  for i in range(len(v_other)) ])

        c = np.dot(n_self,n_other)/(np.linalg.norm(n_self)*np.linalg.norm(n_other))
        return np.arccos(np.clip(c, -1, 1))
