import numpy as np
from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier

from rlbot.agents.base_agent import BaseAgent, SimpleControllerState
from rlbot.utils.structures.game_data_struct import GameTickPacket

import data_builder_inputs as dbi
import data_builder_field as dbf
import models_io

class MLBot(BaseAgent):

    def initialize_agent(self):

        self.model_type = "KNC"
        self.game_type = "2v2"
        self.version = "v1"

        self.model_throttle = models_io.load_model(0, self.model_type, self.game_type, self.version)
        self.model_steer = models_io.load_model(1, self.model_type, self.game_type, self.version)
        self.model_pitch = models_io.load_model(2, self.model_type, self.game_type, self.version)
        self.model_yaw = models_io.load_model(3, self.model_type, self.game_type, self.version)
        self.model_roll = models_io.load_model(4, self.model_type, self.game_type, self.version)
        self.model_jump = models_io.load_model(5, self.model_type, self.game_type, self.version)
        self.model_boost = models_io.load_model(6, self.model_type, self.game_type, self.version)
        self.model_handbrake = models_io.load_model(7, self.model_type, self.game_type, self.version)

        print("models loaded")

        self.field_info = self.get_field_info()
        self.controller_state = SimpleControllerState()


    def __update_controller_state(self, packet: GameTickPacket):

        my_index = self.index
        mate_index = (self.index+2)%4
        opp1_index = (self.index+1)%4
        opp2_index = (self.index+3)%4

        data = [np.array(dbf.get_field_entry_2v2_v1(packet, self.field_info, my_index, mate_index, opp1_index, opp2_index))]

        self.controller_state.throttle = self.model_throttle.predict(data)[0]
        self.controller_state.steer = self.model_steer.predict(data)[0]
        self.controller_state.pitch = self.model_pitch.predict(data)[0]
        self.controller_state.yaw = self.model_yaw.predict(data)[0]
        self.controller_state.roll = self.model_roll.predict(data)[0]
        self.controller_state.jump = self.model_jump.predict(data)[0]
        self.controller_state.boost = self.model_boost.predict(data)[0]
        self.controller_state.handbrake = self.model_handbrake.predict(data)[0]



    def get_output(self, packet: GameTickPacket) -> SimpleControllerState:
        if (packet.game_info.is_round_active):
            self.__update_controller_state(packet)
        return self.controller_state
