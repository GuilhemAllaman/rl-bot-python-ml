import numpy as np
from rlbot.agents.base_agent import SimpleControllerState

def booleanize(raw) -> np.int8:
    return np.int8(round(raw))

def normalize_float(raw) -> np.int8:
    return np.int8(round(raw*100))



def controller_state_entry(controller_state: SimpleControllerState):
    return np.asarray([
            booleanize(controller_state.throttle),
            booleanize(controller_state.steer),
            booleanize(controller_state.pitch),
            booleanize(controller_state.yaw),
            controller_state.roll,
            controller_state.jump,
            controller_state.boost,
            controller_state.handbrake
        ], dtype=np.int8)

def print_controller_state(controller_state: SimpleControllerState):
    print("throttle: {:>3} * steer: {:>3} * pitch: {:>3} * yaw: {:>3} * roll: {:>3} * jump: {:>3} * boost: {:>3} * handbrake: {:>3}".format(
        controller_state.throttle,
        controller_state.steer,
        controller_state.pitch,
        controller_state.yaw,
        controller_state.roll,
        controller_state.jump,
        controller_state.boost,
        controller_state.handbrake
    ))
