import os
import datetime
import pickle

import numpy as np

data_save_dir_1v1 = "./data/1v1"
data_save_dir_2v2 = "./data/2v2"

def save_data(data, frames, data_type:str, directory:str):
    now = datetime.datetime.now()
    file_name = now.strftime("%Y%m%d_%H%M") + "_" + data_type + "_data_" + str(frames) + "frames.pkl"
    path = os.path.join(directory, file_name)
    pickle.dump(data, open(path,"wb"))

def save_data_1v1(data, frames, data_type:str, version:str):
    save_data(data, frames, data_type, os.path.join(data_save_dir_1v1, version))

def save_data_2v2(data, frames, data_type:str, version:str):
    save_data(data, frames, data_type, os.path.join(data_save_dir_2v2, version))
