import numpy as np
import math

from rlbot.utils.structures.game_data_struct import GameTickPacket
from rlbot.messages.flat.FieldInfo import FieldInfo
import rlbot.utils.game_state_util as gs_util

from rl_maths import Vector3

def proper_field_position(raw, div_factor: int) -> np.int16:
    return np.int16(round(raw/div_factor))

def compute_car_nose_orientation(pitch, yaw, roll) -> Vector3:
    #nose_x = -1 * math.cos(pitch) * math.cos(yaw)
    nose_x = math.cos(pitch) * math.cos(yaw)
    nose_y = math.cos(pitch) * math.sin(yaw)
    nose_z = math.sin(pitch)
    return Vector3(nose_x, nose_y, nose_z)

def compute_car_roof_orientation(pitch, yaw, roll) -> Vector3:
    roof_x = math.cos(roll) * math.sin(pitch) * math.cos(yaw) + math.sin(roll) * math.sin(yaw);
    roof_y = math.cos(yaw) * math.sin(roll) - math.cos(roll) * math.sin(pitch) * math.sin(yaw);
    roof_z = math.cos(roll) * math.cos(pitch);
    return Vector3(roof_x, roof_y, roof_z)



def get_field_entry_1v1_v1(packet:GameTickPacket, field_info:FieldInfo, myIndex:int, oppIndex:int):

    ball_physics = packet.game_ball.physics
    human = packet.game_cars[myIndex]
    human_physics = human.physics
    opp = packet.game_cars[oppIndex]
    opp_physics = opp.physics

    return np.asarray([

            proper_field_position(ball_physics.location.x, 100),     #ball x
            proper_field_position(ball_physics.location.y, 100),     #ball y
            proper_field_position(ball_physics.location.z, 100),     #ball z
            proper_field_position(ball_physics.velocity.x, 100),     #ball velocity x
            proper_field_position(ball_physics.velocity.y, 100),     #ball velocity y
            proper_field_position(ball_physics.velocity.z, 100),     #ball velocity z

            round(human.boost),
            human.jumped,
            human.double_jumped,
            human.has_wheel_contact,
            human.is_demolished,
            proper_field_position(human_physics.location.x, 100),     #human x
            proper_field_position(human_physics.location.y, 100),     #human y
            proper_field_position(human_physics.location.z, 100),     #human z
            proper_field_position(human_physics.velocity.x, 100),     #human velocity x
            proper_field_position(human_physics.velocity.y, 100),     #human velocity y
            proper_field_position(human_physics.velocity.z, 100),     #human velocity z
            proper_field_position(human_physics.rotation.pitch, 10),     #human rotation pitch
            proper_field_position(human_physics.rotation.yaw, 10),     #human rotation yaw
            proper_field_position(human_physics.rotation.roll, 10),     #human rotation roll

            proper_field_position(opp_physics.location.x, 100),     #opponent x
            proper_field_position(opp_physics.location.y, 100),     #opponent y
            proper_field_position(opp_physics.location.z, 100),     #opponent z
            proper_field_position(opp_physics.velocity.x, 100),     #opponent velocity x
            proper_field_position(opp_physics.velocity.y, 100),     #opponent velocity y
            proper_field_position(opp_physics.velocity.z, 100)      #opponent velocity z

        ], dtype=np.int16)

def get_field_entry_1v1_v2(packet:GameTickPacket, field_info:FieldInfo, my_index:int, opp_index:int):

    ball_physics = packet.game_ball.physics
    ball_location = Vector3.from_base(ball_physics.location)
    ball_velocity = Vector3.from_base(ball_physics.velocity)

    human = packet.game_cars[my_index]
    human_physics = human.physics
    human_location = Vector3.from_base(human_physics.location)
    human_nose_orientation = compute_car_nose_orientation(human_physics.rotation.pitch, human_physics.rotation.yaw, human_physics.rotation.roll)

    opp = packet.game_cars[opp_index]
    opp_physics = opp.physics
    opp_location = Vector3.from_base(opp_physics.location)
    opp_nose_orientation = compute_car_nose_orientation(opp_physics.rotation.pitch, opp_physics.rotation.yaw, opp_physics.rotation.roll)

    human_goal_location = Vector3.from_base(field_info.goals[human.team].location)
    opp_goal_location = Vector3.from_base(field_info.goals[opp.team].location)

    array = np.asarray([

        #distance ball - human's goal
        proper_field_position(ball_location.distance_2d(human_goal_location), 100),
        #ball z-location
        proper_field_position(ball_location.z, 100),
        #ball velocity
        proper_field_position(Vector3.from_base(ball_physics.velocity).length_2d(), 10),


        #distance human - ball (3d)
        proper_field_position(human_location.distance_2d(ball_location), 100),
        #angle human - ball (2d)
        proper_field_position(ball_location.minus(human_location).angle_2d(human_nose_orientation), 0.1),
        #distance human - human's goal (3d)
        proper_field_position(human_location.distance_2d(human_goal_location), 100),
        #angle human - human's goal (2d)
        proper_field_position(human_goal_location.minus(human_location).angle_2d(human_nose_orientation), 0.1),
        #human z-location
        proper_field_position(human_location.z, 100),
        #human velocity (2d)
        proper_field_position(Vector3.from_base(human_physics.velocity).length_2d(), 10),



        #distance opponent - ball (2d)
        proper_field_position(opp_location.distance_2d(ball_location), 100),
        #angle opponent - ball (2d)
        proper_field_position(ball_location.minus(opp_location).angle_2d(opp_nose_orientation), 0.1),
        #distance opponent - human's goal (2d)
        proper_field_position(opp_location.distance_2d(human_goal_location), 100),
        #angle human - human's goal (2d)
        proper_field_position(human_goal_location.minus(opp_location).angle_2d(opp_nose_orientation), 0.1),
        #opp z-location
        proper_field_position(opp_location.z, 100),
        #opponent's velocity (2d)
        proper_field_position(Vector3.from_base(opp_physics.velocity).length_2d(), 10),


        round(human.boost),
        human.jumped,
        human.double_jumped,

        #human rotation values - NO YAW
        proper_field_position(human_physics.rotation.pitch, 10),
        proper_field_position(human_physics.rotation.roll, 10),

    ], dtype=np.int16)

    return array


def get_field_entry_2v2_v1(packet: GameTickPacket, field_info:FieldInfo, my_index:int, mate_index:int, opp1_index:int, opp2_index:int):

    ball_physics = packet.game_ball.physics
    ball_location = Vector3.from_base(ball_physics.location)
    ball_velocity = Vector3.from_base(ball_physics.velocity)

    me = packet.game_cars[my_index]
    my_physics = me.physics

    mate_physics = packet.game_cars[my_index].physics
    opp1_physics = packet.game_cars[my_index].physics
    opp2_physics = packet.game_cars[my_index].physics

    my_goal_location = Vector3.from_base(field_info.goals[me.team].location)
    opp_goal_location = Vector3.from_base(field_info.goals[(me.team+1)%2].location)

    result = [
        #distance ball - my goal (2d)
        proper_field_position(ball_location.distance_2d(my_goal_location), 10),
        #ball z-location
        proper_field_position(ball_location.z, 10),
        #ball velocity (3d)
        proper_field_position(Vector3.from_base(ball_physics.velocity).length_3d(), 100),
    ]

    result.extend(get_player_field_infos(my_physics, ball_physics, my_goal_location))
    result.extend(get_player_field_infos(mate_physics, ball_physics, my_goal_location))
    result.extend(get_player_field_infos(opp1_physics, ball_physics, my_goal_location))
    result.extend(get_player_field_infos(opp2_physics, ball_physics, my_goal_location))

    result.extend([

        round(me.boost),
        me.jumped,
        me.double_jumped,

        #player rotation values - NO YAW
        proper_field_position(my_physics.rotation.pitch, 10),
        proper_field_position(my_physics.rotation.roll, 10),
    ])

    return np.asarray(result, dtype=np.int16)

def get_player_field_infos(player_physics, ball_physics, goal_location):

    ball_location = Vector3.from_base(ball_physics.location)

    player_location = Vector3.from_base(player_physics.location)
    player_nose_orientation = compute_car_nose_orientation(player_physics.rotation.pitch, player_physics.rotation.yaw, player_physics.rotation.roll)

    return [
        #distance player - ball (3d)
        proper_field_position(player_location.distance_3d(ball_location), 10),
        #angle player - ball (2d)
        proper_field_position(ball_location.minus(player_location).angle_2d(player_nose_orientation), 0.1),
        #distance player - goal (3d)
        proper_field_position(player_location.distance_3d(goal_location), 10),
        #angle player -  goal (2d)
        proper_field_position(goal_location.minus(player_location).angle_2d(player_nose_orientation), 0.1),
        #player z-location
        proper_field_position(player_location.z, 10),
        #player velocity (2d)
        proper_field_position(Vector3.from_base(player_physics.velocity).length_2d(), 100)
    ]
