from rlbot.agents.base_agent import BaseAgent, SimpleControllerState
from rlbot.utils.structures.game_data_struct import GameTickPacket
import pygame
import math

import data_builder_inputs as dbi
import data_builder_field as dbf
import data_io

FRAMES_PER_FILE = 2000

class DataCollectorBot(BaseAgent):

    def initialize_agent(self):

        self.frame = 0

        self.controller_state = SimpleControllerState()
        self.empty_controller_state = SimpleControllerState()

        self.c_entries = []
        self.f_entries = []
        self.game_over = False

        self.field_info = self.get_field_info()

        pygame.init()
        pygame.joystick.init()
        self.controller = pygame.joystick.Joystick(0)
        self.controller.init()
        print('Listening on joystick :', self.controller.get_name())


    def update_human_inputs(self):

        pygame.event.pump()

        c = self.controller
        self.controller_state.throttle = - c.get_axis(2)
        self.controller_state.steer = c.get_axis(0)
        self.controller_state.pitch = c.get_axis(1)
        self.controller_state.yaw = c.get_axis(0)
        self.controller_state.roll = c.get_button(5) - c.get_button(4)
        self.controller_state.jump = (c.get_button(0) == 1)
        self.controller_state.boost = (c.get_button(1) == 1)
        self.controller_state.handbrake = (c.get_button(2) == 1)


    def get_output(self, packet: GameTickPacket) -> SimpleControllerState:

        if (packet.game_info.is_round_active):
            self.update_human_inputs()
            c_entry = dbi.controller_state_entry(self.controller_state)
            #f_entry = dbf.get_field_entry_1v1(packet, self.field_info, 0, 1)
            #f_entry = dbf.get_field_entry_1v1_v2(packet, self.field_info, 0, 1)
            f_entry = dbf.get_field_entry_2v2_v1(packet, self.field_info, 0, 2, 1, 3)
            self.c_entries.append(c_entry)
            self.f_entries.append(f_entry)
            self.frame+=1

        if (packet.game_info.is_match_ended and not self.game_over):
            data_io.save_data_2v2(self.c_entries, self.frame, "inputs", "v1")
            data_io.save_data_2v2(self.f_entries, self.frame, "field", "v1")
            print("game over, ", self.frame, " frames stored")
            self.game_over = True

        return self.empty_controller_state
