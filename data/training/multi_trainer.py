import numpy as np
import glob
import os
import pickle

metas = []
all_features = []
all_labels = []

game_type = "2v2"
version = "v1"

for file in glob.glob(os.path.join("../" + game_type + "/" + version, "*field*.pkl")):
    all_features.extend(pickle.load(open(file,"rb")))

for file in glob.glob(os.path.join("../" + game_type + "/" + version, "*inputs*.pkl")):
    all_labels.extend(pickle.load(open(file,"rb")))

from sklearn.neighbors import KNeighborsClassifier
from sklearn import tree
from sklearn import linear_model
knc = KNeighborsClassifier()
dtc = tree.DecisionTreeClassifier()
sgd = linear_model.SGDClassifier(max_iter=1000, tol=1e-3, shuffle=True, loss="log", warm_start=False, verbose=0)


from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

'''for i in range (0, len(all_features)):
    
    features = np.array(all_features[i])
    labels_multidim = np.array(all_labels[i])'''
    
for input in range (0, np.array(all_labels).shape[1]):
    
    labels = [tmp[input] for tmp in all_labels]
    x_train, x_test, y_train, y_test = train_test_split(all_features, labels, test_size = .2)
    
    dtc.fit(all_features, labels)
    knc.fit(all_features, labels)
    #sgd.fit(all_features, labels)
    
    print("-- processing input n°", input)
    model_file_name_dtc = "../models/" + game_type + "/" + version + "/model_DTC_input" + str(input) + ".sav"
    pickle.dump(dtc, open(model_file_name_dtc, 'wb'))
    model_file_name_knc = "../models/" + game_type + "/" + version + "/model_KNC_input" + str(input) + ".sav"
    pickle.dump(knc, open(model_file_name_knc, 'wb'))
    
    predictionsDTC = dtc.predict(x_test)
    predictionsKNC = knc.predict(x_test)
    #predictionsSGD = sgd.predict(x_test)
    
    print(input, " - DTC accuraccy : ", accuracy_score(y_test, predictionsDTC))
    print(input, " - KNC accuraccy : ", accuracy_score(y_test, predictionsKNC))
    #print(input, " - SGD accuraccy : ", accuracy_score(y_test, predictionsSGD))
    
print("Done.")